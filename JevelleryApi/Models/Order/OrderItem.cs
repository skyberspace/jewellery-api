﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{
    public enum OrderItemStatus
    {
        Beklemede, Hazirlaniyor, Hazir
    }
    public class OrderItem : EntityBase
    {
        [Key]
        public int Id { get; set; }

        #region Order Property
        private int _OrderId = default(int);
        public int OrderId
        {
            get
            {
                return _OrderId;
            }
            set
            {
                _OrderId = value;
            }
        }

        private Order _Order = default(Order);
        [JsonIgnore]
        [ForeignKey("OrderId")]
        public virtual Order Order
        {
            get
            {
                return _Order;
            }
            set
            {
                _Order = value;
            }
        }
        #endregion

        #region Product Property
        private int? _ProductId = null;
        public int? ProductId
        {
            get
            {
                return _ProductId;
            }
            set
            {
                _ProductId = value;
            }
        }

        private Product _Product = default(Product);
        [ForeignKey("ProductId")]
        public virtual Product Product
        {
            get
            {
                return _Product;
            }
            set
            {
                _Product = value;
            }
        }
        #endregion

        #region User Property
        private int? _UserId = null;
        public int? UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                _UserId = value;
            }
        }

        private User _User = default(User);
        [JsonIgnore]
        [ForeignKey("UserId")]
        public virtual User User
        {
            get
            {
                return _User;
            }
            set
            {
                _User = value;
            }
        }
        #endregion

        public double OriginalUnitWeight { get; set; }
        public double CalculatedUnitWeight { get; set; }
        public double OriginalCarat { get; set; }
        public double SelectedCarat { get; set; }
        public double Quantity { get; set; }
        public double LaborCost { get; set; }
        public string Attributes { get; set; }
        public double MaterialPrice { get; set; }
        public OrderItemStatus Status { get; set; }
        public string Note { get; set; }
    }
}