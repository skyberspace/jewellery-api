﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{
    public class Order : EntityBase
    {
        [Key]
        public int Id { get; set; }

        #region User Property
        private int? _UserId = null;
        public int? UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                _UserId = value;
            }
        }

        private User _User = default(User);
        [ForeignKey("UserId")]
        public virtual User User
        {
            get
            {
                return _User;
            }
            set
            {
                _User = value;
            }
        }
        #endregion

        public virtual List<OrderItem> OrderItems { get; set; }
        public OrderStatus Status { get; set; }
        public double OnsPrice { get; set; }
        public string Currency { get; set; }
        public string Note { get; set; }
    }
}