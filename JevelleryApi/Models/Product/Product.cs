﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{
    public class Product : EntityBase
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        #region Category Property
        private int? _CategoryId = null;
        public int? CategoryId
        {
            get
            {
                return _CategoryId;
            }
            set
            {
                _CategoryId = value;
            }
        }

        private Category _Category = default(Category);
        [JsonIgnore]
        [ForeignKey("CategoryId")]
        public virtual Category Category
        {
            get
            {
                return _Category;
            }
            set
            {
                _Category = value;
            }
        }
        #endregion

        [JsonIgnore]
        public virtual List<ProductImage> Images { get; set; }

        public double Weight { get; set; }
        public double Carat { get; set; }
        public string Description { get; set; }
        public bool HasCarat { get; set; } = true;
        public bool IsShowcased { get; set; }
        public bool Active { get; set; }
        public int? AtolyeId { get; set; }
    }
}