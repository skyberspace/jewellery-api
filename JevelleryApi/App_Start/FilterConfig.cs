﻿using JevelleryApi.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Linq;

namespace JevelleryApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }


    public class IsApprovedFilter : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = UserProviderFromActionExecutingContext.Provide(filterContext, out var controller);
            if (user != null)
            {
                if (user.IsAtolye)
                {
                    filterContext.Result = controller.JsonResult(new { success = false, message = "Atölye Hesabı İle Ürünleri Görüntüleyemezsiniz!", redirectToLogin = true });
                    return;
                }
                if (user.Status != UserStatus.Onayli)
                {
                    filterContext.Result = controller.JsonResult(new { success = false, message = "Hesabınız Henüz Onaylanmamış!", redirectToLogin = true });
                    return;
                }
            }
        }
    }

    public class IsAtolyeFilter : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = UserProviderFromActionExecutingContext.Provide(filterContext, out var controller);
            if (user != null)
            {
                if (!user.IsAtolye)
                {
                    filterContext.Result = controller.JsonResult(new { success = false, message = "Hesap Türünüz Bu Sayfayı Görüntüleyemez!", redirectToLogin = true });
                    return;
                }
            }
        }
    }

    public class IsAdminFilter : ActionFilterAttribute, IActionFilter
    {
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = UserProviderFromActionExecutingContext.Provide(filterContext, out var controller);
            if (user != null)
            {
                if (!user.IsAdmin)
                {
                    filterContext.Result = controller.JsonResult(new { success = false, message = "Hesap Türünüz Bu Sayfayı Görüntüleyemez!", redirectToLogin = true });
                    return;
                }
            }
        }
    }
}
