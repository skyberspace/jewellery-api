﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{
    public class JevelleryDB : ExtendedDbContextWithAuth<User>
    {
        public JevelleryDB() : base(ConnectionStringProvider.GetConnectionString())
        {
        }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductImage> ProductImages { get; set; }
        public virtual DbSet<CartItem> CartItems { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderItem> OrderItems { get; set; }


    }
    public enum UserStatus
    {
        Beklemede, Onayli
    }
    public enum OrderStatus
    {
        Beklemede, Onaylandi, Hazirlaniyor, Hazir
    }
    public class User : ExtendedUser
    {
        public string Token { get; set; }
        public UserStatus Status { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsAtolye { get; set; }
        public string Name { get; set; }
        //public int AtolyeId { get; set; }
    }
}