﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using M6T.EntitySkeleton.Extensions;
using JevelleryApi.Models;
using JevelleryApi.Models.Providers;

namespace JevelleryApi.Controllers
{
    [IsAtolyeFilter]
    public class AtolyeController : ControllerExtensions
    {

        [HttpPost]
        public ActionResult GetOrders(int statusFilter, Pagination pagination, string userToken)
        {
            var userQ = db.ExtendedUsers.Where(x => x.Token == userToken).ToList();
            if (userQ.Count == 0)
                return RedirectToLogin();

            var user = userQ.First();

            var q = db.Orders.Include(x => x.OrderItems).Where(x => x.OrderItems.Any(i => i.Product.AtolyeId == user.Id));

            if (statusFilter != -1)
            {
                q = q.Where(x => x.Status == (OrderStatus)statusFilter);
            }

            var ordered = q.OrderByDescending(x => x.CreatedAt);


            var itemCount = ordered.Count();
            var pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(itemCount) / Convert.ToDouble(pagination.PageSize)));
            var paged = q.Skip(pagination.Page * pagination.PageSize).Take(pagination.PageSize);

            var items = paged.ToListAsync().Result.Cast<Order>().ToList();

            //Sadece atölyenin kendine ait olan ürünleri filtere
            foreach (var item in items)
            {
                item.OrderItems = item.OrderItems.Where(x => x.Product.AtolyeId == user.Id).ToList();
            }

            return Json(new
            {
                orders = items,
                pagination = new Pagination
                {
                    ItemCount = itemCount,
                    PageCount = pageCount,
                    Page = pagination.Page,
                    PageSize = pagination.PageSize,
                }
            });
        }


        [HttpPost]
        public ActionResult GetOrder(int orderId, string userToken)
        {
            var userQ = db.ExtendedUsers.Where(x => x.Token == userToken).ToList();
            if (userQ.Count == 0)
                return RedirectToLogin();

            var user = userQ.First();

            var order = db.Orders.Include(x => x.OrderItems).Where(x => x.Id == orderId).FirstOrDefault();

            if (order == null)
                return GenerateError("Sipariş Bulunamadı");

            order.OrderItems = order.OrderItems.Where(x => x.Product.AtolyeId == user.Id).ToList();

            if (order.OrderItems.Count == 0)
                return GenerateError("Atölyenize tanımlı böyle bir sipariş bulunamadı");

            return Json(order);
        }


        [HttpPost]
        public ActionResult SetOrderItemStatus(int orderItemId, int status, string userToken)
        {
            var userQ = db.ExtendedUsers.Where(x => x.Token == userToken).ToList();
            if (userQ.Count == 0)
                return RedirectToLogin();

            var user = userQ.First();



            var orderItem = db.OrderItems.Find(status);

            if (orderItem.UserId != user.Id)
                return GenerateError("Yetki Hatası!");

            orderItem.Status = (OrderItemStatus)status;

            db.SaveChanges();
            return GenerateSuccess();
        }
    }
}