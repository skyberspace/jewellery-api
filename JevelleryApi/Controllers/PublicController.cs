﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using M6T.EntitySkeleton.Extensions;
using JevelleryApi.Models;

namespace JevelleryApi.Controllers
{
    public class PublicController : ControllerExtensions
    {

        [HttpPost]
        public ActionResult GetCategories(int? parentCategoryId)
        {
            var categories = db.Categories.Where(x => x.ParentCategoryId == parentCategoryId && x.Active).ToList();
            return Json(categories);
        }


        /// <summary>
        /// Meüde kullanılacak kategorilerin listesini döndürür.
        /// </summary>
        [HttpPost]
        public ActionResult GetCategoriesForMenu()
        {
            return Json(db.Categories.Where(x => x.ShowOnMenu && x.Active));
        }

        /// <summary>
        /// Showcase seçilen ürünleri listeler. Sayfalandırma ve isimden filtrelemeyi destekler.
        /// </summary>
        [HttpPost]
        public ActionResult GetShowcasedProducts(string filter, Pagination pagination)
        {
            var q = db.Products.Where(x => x.IsShowcased && x.Active);


            if (!string.IsNullOrEmpty(filter))
                q = q.Where(x => x.Name.ToLower().Contains(filter.ToLower()));

            var itemCount = q.Count();
            var paged = q.Skip(pagination.Page * pagination.PageSize).Take(pagination.PageSize);
            var pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(itemCount) / Convert.ToDouble(pagination.PageSize)));


            var products = paged.ToListAsync().Result.Cast<Product>().ToList();

            return Json(new
            {
                products,
                pagination = new Pagination()
                {
                    Page = pagination.Page,
                    PageSize = pagination.PageSize,
                    ItemCount = itemCount,
                    PageCount = pageCount,
                }
            });
        }
    }
}