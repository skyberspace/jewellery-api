﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{
    public class GoldCalculator
    {
        static Dictionary<double, double> GoldCaratValues
            = new Dictionary<double, double> {
            {24,1000 },
            {22,916 },
            {21,875 },
            {20,833 },
            {18,750 },
            {15,625 },
            {14,585 },
            {10,417 },
            {9,375 },
            {8,333 }
        };

        public static double GetCalculatedWeight(double originalWeight, double originalCarat, double targetCarat)
        {
            return originalWeight * (GoldCaratValues[targetCarat] / GoldCaratValues[originalCarat]);
        }

        public static double GetMaterialPrice(double onsPrice, OrderItem orderItem)
        {
            var totalGram = (orderItem.CalculatedUnitWeight * orderItem.Quantity);
            var totalKg = totalGram / 1000;
            var onsWeight = (31.15 * totalKg);
            return onsPrice * onsWeight;
        }

    }
}