﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{
    [DisableSoftDelete]
    public class CartItem : EntityBase
    {
        [Key]
        public int Id { get; set; }

        #region User Property
        private int _UserId = default(int);
        public int UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                _UserId = value;
            }
        }

        private User _User = default(User);
        [JsonIgnore]
        [ForeignKey("UserId")]
        public virtual User User
        {
            get
            {
                return _User;
            }
            set
            {
                _User = value;
            }
        }
        #endregion

        #region Product Property
        private int _ProductId = default(int);
        public int ProductId
        {
            get
            {
                return _ProductId;
            }
            set
            {
                _ProductId = value;
            }
        }

        private Product _Product = default(Product);
        [JsonIgnore]
        [ForeignKey("ProductId")]
        public virtual Product Product
        {
            get
            {
                return _Product;
            }
            set
            {
                _Product = value;
            }
        }
        #endregion

        public string Attributes { get; set; }
        public double SelectedCarat { get; set; }
        public double Quantity { get; set; }
        public string Note { get; set; }
    }
}