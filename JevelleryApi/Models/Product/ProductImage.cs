﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{

    public class ProductImage : EntityBase
    {
        [Key]
        public int Id { get; set; }

        #region Product Property
        private int _ProductId = default(int);
        public int ProductId
        {
            get
            {
                return _ProductId;
            }
            set
            {
                _ProductId = value;
            }
        }

        private Product _Product = default(Product);
        [JsonIgnore]
        [ForeignKey("ProductId")]
        public virtual Product Product
        {
            get
            {
                return _Product;
            }
            set
            {
                _Product = value;
            }
        }
        #endregion

        public string Path { get; set; }
        public string Slug { get; set; }
        public string Alt { get; set; }
        public bool Cover { get; set; }
    }
}