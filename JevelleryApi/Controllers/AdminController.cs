﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using M6T.EntitySkeleton.Extensions;
using JevelleryApi.Models;
using JevelleryApi.Models.Providers;

namespace JevelleryApi.Controllers
{
    [IsAdminFilter]
    public class AdminController : ControllerExtensions
    {

        [HttpPost]
        public ActionResult ListOrders(string filter, int statusFilter, Pagination pagination)
        {
            var q = db.Orders.Include(x => x.OrderItems).Where(x => true);
            if (!string.IsNullOrWhiteSpace(filter))
            {
                filter = filter.ToLower();
                q = q.Where(x =>
                    x.User.Email.ToLower().Contains(filter) ||
                    x.User.Name.ToLower().Contains(filter)
                );
            }
            if (statusFilter != -1)
            {
                q = q.Where(x => x.Status == (OrderStatus)statusFilter);
            }

            var ordered = q.OrderByDescending(x => x.CreatedAt);


            var itemCount = ordered.Count();
            var pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(itemCount) / Convert.ToDouble(pagination.PageSize)));
            var paged = ordered.Skip(pagination.Page * pagination.PageSize).Take(pagination.PageSize);

            var items = paged.ToListAsync().Result.Cast<Order>().ToList();

            return Json(new
            {
                orders = items,
                pagination = new Pagination
                {
                    ItemCount = itemCount,
                    PageCount = pageCount,
                    Page = pagination.Page,
                    PageSize = pagination.PageSize,
                }
            });

        }

        [HttpPost]
        public ActionResult GetOrderDetail(int orderId)
        {
            var order = db.Orders.Include(x => x.OrderItems).Where(x => x.Id == orderId).FirstOrDefault();
            if (order == null)
                return GenerateError("Sipariş Bulunamadı!");

            return Json(order);
        }

        [HttpPost]
        public ActionResult SaveOrderItems(OrderItem[] items)
        {
            foreach (var item in items)
            {
                db.AddOrUpdateEntity(item);
            }
            db.SaveChanges();
            return GenerateSuccess();
        }


        [HttpPost]
        public ActionResult ChangeOrderCurrency(int orderId)
        {
            var order = db.Orders.Find(orderId);
            if (order.Currency == "USD")
                order.Currency = "EUR";
            else if (order.Currency == "EUR")
                order.Currency = "USD";
            db.SaveChanges();
            order = CalculateOnsPriceInner(orderId);
            return Json(order);
        }

        [HttpPost]
        public ActionResult SetOrderItemStatus(int orderItemId, int status)
        {
            var order = db.OrderItems.Find(orderItemId);
            order.Status = (OrderItemStatus)status;
            db.SaveChanges();
            return GenerateSuccess();
        }

        [HttpPost]
        public ActionResult SetOrderStatus(int orderId, int status)
        {
            var order = db.Orders.Find(orderId);
            order.Status = (OrderStatus)status;
            db.SaveChanges();
            return GenerateSuccess();
        }

        [HttpPost]
        public ActionResult GetCurrentOnsPrice(string currency)
        {
            return Json(GoldOnsPriceProvider.GetOnsPrice(currency));
        }


        [HttpPost]
        public ActionResult CalculateOnsPrice(int orderID)
        {
            Order order = CalculateOnsPriceInner(orderID);
            return Json(order);
        }

        private Order CalculateOnsPriceInner(int orderID)
        {
            var order = db.Orders.Find(orderID);
            var new_price = GoldOnsPriceProvider.GetOnsPrice(order.Currency);
            order.OnsPrice = new_price;
            foreach (var item in order.OrderItems)
            {
                var materialPrice = GoldCalculator.GetMaterialPrice(order.OnsPrice, item);
                item.MaterialPrice = materialPrice;
            }
            db.SaveChanges();
            return order;
        }

        [HttpPost]
        public ActionResult SaveOrder(Order order)
        {
            db.AddOrUpdateEntity(order);
            db.SaveChanges();
            return GenerateSuccess();
        }

        [HttpPost]
        public ActionResult ListUsers(string filter, int statusFilter, bool isAdmin, bool isUser, bool isAtolye, Pagination pagination)
        {
            var q = db.ExtendedUsers.Where(x => true);
            if (!string.IsNullOrWhiteSpace(filter))
            {
                filter = filter.ToLower();
                q = q.Where(x =>
                    x.Name.ToLower().Contains(filter) ||
                    x.Email.ToLower().Contains(filter)
                );
            }
            if (statusFilter != -1)
            {
                q = q.Where(x => x.Status == (UserStatus)statusFilter);
            }
            if (isAdmin)
                q = q.Where(x => x.IsAdmin);
            if (isUser)
                q = q.Where(x => !x.IsAdmin && !x.IsAtolye);
            if (isAtolye)
                q = q.Where(x => x.IsAtolye);

            var ordered = q.OrderByDescending(x => x.CreatedAt);

            var itemCount = ordered.Count();
            var pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(itemCount) / Convert.ToDouble(pagination.PageSize)));
            var paged = ordered.Skip(pagination.Page * pagination.PageSize).Take(pagination.PageSize);

            var items = paged.ToListAsync().Result.Cast<User>().ToList();

            return Json(new
            {
                users = items,
                pagination = new Pagination
                {
                    ItemCount = itemCount,
                    PageCount = pageCount,
                    Page = pagination.Page,
                    PageSize = pagination.PageSize,
                }
            });
        }

        [HttpPost]
        public ActionResult GetUserDetail(int id)
        {
            var user = db.ExtendedUsers.Where(x => x.Id == id).FirstOrDefault();
            return Json(user);
        }

        [HttpPost]
        public ActionResult SaveUser(User user)
        {
            db.AddOrUpdateEntity(user);

            db.SaveChanges();
            return GenerateSuccess();
        }

        [HttpPost]
        public ActionResult ListProducts(string filter, int categoryFilter, int activeFilter, Pagination pagination)
        {
            var q = db.Products.Where(x => true);

            filter = filter.ToLower();
            if (!string.IsNullOrEmpty(filter))
                q = q.Where(x => x.Name.ToLower().Contains(filter));

            if (categoryFilter != -1)
                q = q.Where(x => x.CategoryId == categoryFilter);

            if (activeFilter != -1)
            {
                if (activeFilter == 1)
                    q = q.Where(x => x.Active);
                else
                    q = q.Where(x => !x.Active);
            }

            var ordered = q.OrderByDescending(x => x.CreatedAt);


            var itemCount = ordered.Count();
            var pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(itemCount) / Convert.ToDouble(pagination.PageSize)));
            var paged = ordered.Skip(pagination.Page * pagination.PageSize).Take(pagination.PageSize);

            var items = paged.ToListAsync().Result.Cast<Product>().ToList();

            return Json(new
            {
                products = items,
                pagination = new Pagination
                {
                    ItemCount = itemCount,
                    PageCount = pageCount,
                    Page = pagination.Page,
                    PageSize = pagination.PageSize,
                }
            });


        }

        [HttpPost]
        public ActionResult SaveProduct(Product product, List<ProductImage> images)
        {
            db.AddOrUpdateEntity(product);
            db.SaveChanges();
            foreach (var image in images)
            {
                image.ProductId = product.Id;
            }
            db.SaveChanges();

            return Json(new { id = product.Id });
        }
    }
}