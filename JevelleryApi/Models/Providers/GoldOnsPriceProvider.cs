﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using HtmlAgilityPack;
namespace JevelleryApi.Models.Providers
{
    public class GoldOnsPriceProvider
    {
        public static double GetOnsPrice(string currency)
        {
            string html = "";
            using (WebClient client = new WebClient())
            {
                html = client.DownloadString("https://altin.in/");
            }
            var document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(html);

            var onsfiyat = Convert.ToDouble(
                document.GetElementbyId("ofiy").InnerText
                .Trim('$')
                .Replace(
                    CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator,
                    CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator
                    )
                );

            var eurParite = Convert.ToDouble(
                document.GetElementbyId("pfiy").InnerText
                 .Trim('$')
                .Replace(
                    CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator,
                    CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator
                    )
                );

            if (currency.ToLower() == "usd")
                return onsfiyat;
            else
                return onsfiyat / eurParite;
        }
    }


    public static class HtmlAgilityExtensions
    {
        public static List<HtmlAgilityPack.HtmlNode> ChildElements(this HtmlAgilityPack.HtmlNode node)
        {
            return node.ChildNodes.GetOnlyElements();
        }
        public static List<HtmlAgilityPack.HtmlNode> GetOnlyElements(this HtmlNodeCollection collection)
        {
            return collection.Where(x => x.NodeType == HtmlAgilityPack.HtmlNodeType.Element).ToList();
        }

        public static List<HtmlAgilityPack.HtmlNode> GetByTagName(this HtmlNode node, string tagName, string className = null, string id = null)
        {
            var ret = node.Descendants(tagName).ToList();
            if (!string.IsNullOrWhiteSpace(className))
            {
                ret = ret.Where(x => x.GetAttributeValue("class", "") == className).ToList();
            }
            if (!string.IsNullOrWhiteSpace(id))
            {
                ret = ret.Where(x => x.Id == id).ToList();
            }
            return ret;
        }

    }
}