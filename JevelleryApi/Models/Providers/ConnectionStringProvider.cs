﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{
    public class ConnectionStringProvider
    {
        private static string ConnectionString = null;
        private static object GetConnectionLock = new object();
        public static string GetConnectionString()
        {
            lock (GetConnectionLock)
            {
                if (string.IsNullOrWhiteSpace(ConnectionString))
                {
                    var currentDir = Directory.GetCurrentDirectory();
                    var filePath = HttpContext.Current.Server.MapPath("~/connection.txt");
                    ConnectionString =  File.ReadAllText(filePath);
                }
                return ConnectionString;
            }
        }
    }
}