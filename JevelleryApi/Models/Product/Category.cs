﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{

    public class Category : EntityBase
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public int? ParentCategoryId { get; set; }
        public bool ShowOnMenu { get; set; }
        public bool Active { get; set; }
    }
}