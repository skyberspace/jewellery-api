﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{
    public class Pagination
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int ItemCount { get; set; }
        public int PageCount { get; set; }

    }
}