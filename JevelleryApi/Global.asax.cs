﻿using JevelleryApi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace JevelleryApi
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ExtendedDbConfiguration.UserIdentityType = UserIdentityType.Integer;
            ExtendedDbConfiguration.AutomaticDatetimeValues = true;
            ExtendedDbConfiguration.PasswordHashAlgorithm = PasswordHashAlgorithm.SHA256;
            ExtendedDbConfiguration.UseCreatedAt = true;
            ExtendedDbConfiguration.UseExtendedMetas = true;
            ExtendedDbConfiguration.UseLoggingTable = false;
            ExtendedDbConfiguration.UsePropertyChangedDictionary = false;
            ExtendedDbConfiguration.UserAuthType = UserAuthType.Email;
            ExtendedDbConfiguration.UserPasswordsSalted = true;
            ExtendedDbConfiguration.UseSoftDelete = true;
            ExtendedDbConfiguration.UseUpdatedAt = true;

            Database.SetInitializer(strategy: new MigrateDatabaseToLatestVersion<JevelleryDB, Migrations.Configuration>());


            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
