﻿using JevelleryApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using M6T.EntitySkeleton.Extensions;

namespace JevelleryApi.Controllers
{
    public class AuthController : ControllerExtensions
    {
        public ActionResult Index()
        {
            db.ExtendedUsers.ToList();
            return Content("");
        }

        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            var user = db.TryLogin(email, password);
            if (user != null)
            {
                if (user.Status == UserStatus.Beklemede)
                    return GenerateError("Hesabınız henüz onaylanmamış!");

                var token = Guid.NewGuid().ToString("N");
                user.Token = token;
                db.SaveChanges();
                return Json(new { status = true, user, token });
            }
            else
            {
                return GenerateError("Kullanıcı Adı Veya Şifre Yanlış");
            }
        }


        [HttpPost]
        public ActionResult Register(string email, string password, string phone, string city, string district, string address, string name)
        {
            email = email.ToLower().Trim();
            var emailQ = db.ExtendedUsers.Where(x => x.Email.ToLower().Trim() == email);
            if (emailQ.Count() > 0)
                return GenerateError("Bu eposta adresi daha önce kullanılmış!");
            var user = new User();
            user.Token = Guid.NewGuid().ToString("N");
            user.Email = email;
            user.Password = password;
            user.Phone = phone;
            user.City = city;
            user.District = district;
            user.Address = address;
            user.Name = name;
            user.Status = UserStatus.Beklemede;
            db.ExtendedUsers.Add(user);
            db.SaveChanges();
            return GenerateSuccess();
        }

        [HttpPost]
        public ActionResult LoginWithToken(string token)
        {
            var q = db.ExtendedUsers.Where(x => x.Token == token);
            if (q.Count() == 0)
            {
                return RedirectToLogin();
            }
            else
            {
                var user = q.First();
                if (user.Status == UserStatus.Beklemede)
                    return GenerateError("Hesabınız henüz onaylanmamış!");
                return Json(new { user, token });
            }
        }

        [HttpPost]
        public ActionResult Logout(string token)
        {
            var q = db.ExtendedUsers.Where(x => x.Token == token);
            if (q.Count() == 0)
            {
                return GenerateSuccess();
            }
            else
            {
                var user = q.First();
                user.Token = new Guid().ToString("N");
                return GenerateSuccess();
            }
        }
    }
}