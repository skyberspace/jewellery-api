﻿using JevelleryApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JevelleryApi.Controllers
{
    public class ControllerExtensions : ExtendedController<JevelleryDB>
    {
        public ActionResult GenerateSuccess()
        {
            return JsonResult(new { success = true });
        }
        public ActionResult RedirectToLogin()
        {
            return JsonResult(new { redirectToLogin = true });
        }

        public ActionResult GenerateError(string message)
        {
            return JsonResult(new { success = false, message });
        }

        public ActionResult Generate404()
        {
            return JsonResult(new { notfound404 = true });
        }
        public new ActionResult Json(object data)
        {
            return JsonResult(data);
        }
    }
}