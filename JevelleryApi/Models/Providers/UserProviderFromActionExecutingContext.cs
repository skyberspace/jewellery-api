﻿using JevelleryApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JevelleryApi
{
    public class UserProviderFromActionExecutingContext
    {
        public static User Provide(ActionExecutingContext filterContext, out ExtendedController<JevelleryDB> controller)
        {
            controller = filterContext.Controller as ExtendedController<JevelleryDB>;

            var usertoken = UserTokenParser.GetCurrentUserTokenFromHttpContext(filterContext.HttpContext.Request, filterContext.HttpContext.Session);

            if (string.IsNullOrWhiteSpace(usertoken.token))
            {
                filterContext.Result = controller.JsonResult(new { success = false, message = "Önce giriş yapmalısınız", redirectToLogin = true });
                return null;
            }

            var q = controller.db.ExtendedUsers.AsNoTracking().Where(x => x.Token == usertoken.token && !x.IsDeleted);
            if (!q.Any())
            {
                filterContext.Result = controller.JsonResult(new { success = false, message = "Önce Giriş Yapmalısınız", redirectToLogin = true });
                return null;
            }

            return q.First();
        }
    }
}