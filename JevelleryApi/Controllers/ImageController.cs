﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using M6T.EntitySkeleton.Extensions;
using JevelleryApi.Models;
using JevelleryApi.Models.Providers;

namespace JevelleryApi.Controllers
{
    public class ImageController : ControllerExtensions
    {
        static Dictionary<string, byte[]> MemCachedImages = new Dictionary<string, byte[]>();
        // GET: Image
        public ActionResult ProductCover(string id)
        {
            var intId = Convert.ToInt32(id);
            var image = db.ProductImages.FirstOrDefault(x => x.ProductId == intId && x.Cover);
            if (image == null)
            {
                return HttpNotFound("Resim bulunamadı");
            }
            var path = Server.MapPath("~/ProductImages/" + image.Path);
            var bytes = new byte[0];
            if (MemCachedImages.ContainsKey(path))
            {
                bytes = MemCachedImages[path];
            }
            else
            {
                bytes = System.IO.File.ReadAllBytes(path);
                MemCachedImages[path] = bytes;
            }
            return File(bytes, "image/jpeg");
        }
    }
}