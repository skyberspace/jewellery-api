﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using M6T.EntitySkeleton.Extensions;
using JevelleryApi.Models;
using JevelleryApi.Models.Providers;

namespace JevelleryApi.Controllers
{
    //Sadece onaylı kullanıcılar bu metodları çalıştırabilir
    [IsApprovedFilter]
    public class PrivateController : ControllerExtensions
    {
        /// <summary>
        /// Kategori Id ve isim filtesi ile sayfalandırma destekli ürün listesi döndürür
        /// </summary>
        [HttpPost]
        public ActionResult GetProductsInCategory(int categoryId, string filter, Pagination pagination)
        {
            var q = db.Products.Where(x => x.CategoryId == categoryId && x.Active);

            if (!string.IsNullOrEmpty(filter))
                q = q.Where(x => x.Name.ToLower().Contains(filter.ToLower()));

            var itemCount = q.Count();

            var ordered = q.OrderByDescending(x => x.CreatedAt);

            var pageCount = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(itemCount) / Convert.ToDouble(pagination.PageSize)));
            var paged = ordered.Skip(pagination.Page * pagination.PageSize).Take(pagination.PageSize);

            var products = paged.ToListAsync().Result.Cast<Product>().ToList();


            return Json(new
            {
                products,
                pagination = new Pagination
                {
                    ItemCount = itemCount,
                    PageCount = pageCount,
                    Page = pagination.Page,
                    PageSize = pagination.PageSize,
                }
            });
        }

        [HttpPost]
        public ActionResult GetProductDetails(int productId)
        {
            var product = db.Products.Include(x => x.Images).First(x => x.Id == productId);
            var images = product.Images;


            return Json(new { product, images });
        }

        [HttpPost]
        public ActionResult AddProductToCart(int productId, double quantity, double carat, string attributes, string userToken)
        {
            var userQ = db.ExtendedUsers.Where(x => x.Token == userToken).ToList();
            if (userQ.Count == 0)
                return RedirectToLogin();

            var user = userQ.First();

            var product = db.Products.Find(productId);
            if (product == null)
                return GenerateError("Ürün bulunamadı!");

            CartItem cartItem = new CartItem()
            {
                Attributes = attributes,
                ProductId = productId,
                Quantity = quantity,
                SelectedCarat = carat,
                UserId = user.Id
            };

            db.CartItems.Add(cartItem);
            db.SaveChanges();

            return GenerateSuccess();
        }

        [HttpPost]
        public ActionResult RemoveProductFromCart(int cartItemId, string userToken)
        {
            var userQ = db.ExtendedUsers.Where(x => x.Token == userToken).ToList();
            if (userQ.Count == 0)
                return RedirectToLogin();

            var user = userQ.First();

            var cartItem = db.CartItems.Find(cartItemId);
            if (cartItem == null)
                return GenerateError("Bir hata oluştu! Silinmek istenen satır mevcut değil.");
            db.CartItems.Remove(cartItem);
            db.SaveChanges();

            return GenerateSuccess();
        }

        [HttpPost]
        public ActionResult ConvertCartToOrder(string userToken, string note)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var userQ = db.ExtendedUsers.Where(x => x.Token == userToken).ToList();
                    if (userQ.Count == 0)
                        return RedirectToLogin();

                    var user = userQ.First();

                    var cartItems = db.CartItems.Include(x => x.Product).Where(x => x.UserId == user.Id).ToList();

                    var order = new Order
                    {
                        Currency = "USD",
                        Status = OrderStatus.Beklemede,
                        UserId = user.Id,
                        OnsPrice = GoldOnsPriceProvider.GetOnsPrice("USD"),
                        Note = note
                    };

                    db.Orders.Add(order);
                    db.SaveChanges();

                    foreach (var cartItem in cartItems)
                    {
                        var calculatedUnitWeight = GoldCalculator.GetCalculatedWeight(cartItem.Product.Weight, cartItem.Product.Carat, cartItem.SelectedCarat);
                        OrderItem orderItem = new OrderItem
                        {
                            Attributes = cartItem.Attributes,
                            ProductId = cartItem.ProductId,
                            Quantity = cartItem.Quantity,
                            SelectedCarat = cartItem.SelectedCarat,
                            UserId = user.Id,
                            LaborCost = 0,
                            OriginalCarat = cartItem.Product.Carat,
                            OriginalUnitWeight = cartItem.Product.Weight,
                            OrderId = order.Id,
                            CalculatedUnitWeight = calculatedUnitWeight,
                            Note = cartItem.Note
                        };
                        var materialPrice = GoldCalculator.GetMaterialPrice(order.OnsPrice, orderItem);
                        orderItem.MaterialPrice = materialPrice;
                        db.OrderItems.Add(orderItem);
                    }
                    db.SaveChanges();

                    transaction.Commit();

                    db.CartItems.RemoveRange(cartItems);
                    db.SaveChanges();
                    return Json(new { success = true, id = order.Id });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return GenerateError(ex.Message);
                }
            }
        }

        [HttpPost]
        public ActionResult GetCartItems(string userToken)
        {
            var userQ = db.ExtendedUsers.Where(x => x.Token == userToken).ToList();
            if (userQ.Count == 0)
                return RedirectToLogin();
            var user = userQ.First();

            var cartItems = db.CartItems.Where(x => x.UserId == user.Id).ToList();

            var productIds = cartItems.Select(x => x.ProductId).Distinct().ToList();

            var products = db.Products.Where(x => productIds.Contains(x.Id)).ToList();
            return Json(cartItems.Select(x => new
            {
                cartItem = x,
                product = products.FirstOrDefault(p => p.Id == x.ProductId)
            }));
        }

        [HttpPost]
        public ActionResult GetOrders(string userToken)
        {
            var userQ = db.ExtendedUsers.Where(x => x.Token == userToken).ToList();
            if (userQ.Count == 0)
                return RedirectToLogin();
            var user = userQ.First();

            var orders = db.Orders.Include(x => x.OrderItems).Where(x => x.UserId == user.Id);

            return Json(orders);
        }

        [HttpPost]
        public ActionResult GetDashboardData(string userToken)
        {

            var userQ = db.ExtendedUsers.Where(x => x.Token == userToken).ToList();
            if (userQ.Count == 0)
                return RedirectToLogin();
            var user = userQ.First();


            var bekleyenCount = db.Orders.Where(x => x.Status == OrderStatus.Beklemede && x.UserId == user.Id).Count();
            var hazirCount = db.Orders.Where(x => x.Status == OrderStatus.Hazir && x.UserId == user.Id).Count();
            var hazirlaniyorCount = db.Orders.Where(x => x.Status == OrderStatus.Hazirlaniyor && x.UserId == user.Id).Count();
            var onaylananCount = db.Orders.Where(x => x.Status == OrderStatus.Onaylandi && x.UserId == user.Id).Count();


            return Json(new { bekleyenCount, onaylananCount, hazirlaniyorCount, hazirCount });
        }
    }
}