﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace JevelleryApi.Models
{
    public class UserTokenParser
    {
        public static (string token, string fullContent) GetCurrentUserTokenFromHttpContext(HttpRequestBase httpRequest, HttpSessionStateBase session)
        {
            //var controller = filterContext.Controller as ExtendedController<Models.TURKENDEKSEntities>;
            var formUserToken = httpRequest.Form["userToken"];
            if (string.IsNullOrWhiteSpace(formUserToken))
            {
                using (var reader = new StreamReader(httpRequest.InputStream))
                {
                    httpRequest.InputStream.Position = 0;
                    var content = reader.ReadToEnd();
                    if (content.Contains("WebKitFormBoundary"))
                        return ("WebKitFormBoundary", content);

                    if (content.Length > 0 && content.Trim()[0] != '{')
                    {
                        return ("WebKitFormBoundary", content);
                    }

                    var userTokenRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<UserTokenRequest>(content);
                    if (userTokenRequest == null)
                    {
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(session["userToken"])))
                        {
                            return (session["userToken"].ToString(), content);
                        }
                        return (null, content);
                    }
                    var usertoken = userTokenRequest.userToken;

                    if (string.IsNullOrWhiteSpace(usertoken))
                    {

                        return (null, content);
                    }

                    session["userToken"] = usertoken;
                    return (usertoken, content);
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(formUserToken))
                {
                    return (null, JsonConvert.SerializeObject(httpRequest.Form));
                }
                return (formUserToken, JsonConvert.SerializeObject(httpRequest.Form));

            }
        }


    }

    public class UserTokenRequest
    {
        public string userToken { get; set; }
    }
}